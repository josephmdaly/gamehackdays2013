8 Windows
==========

A point and click adventure game for Windows Phone 8. Written at
GameHack Days, a 24 hour game development hackathon.


Running
--------

Download and install the windows phone tools here:

[Windows Phone SDK](https://dev.windowsphone.com/en-us/downloadsdk)

Open the solution and run:

GameHackDays2013\GameHackDays2013.sln
