using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace GameHackDays2013
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class LevelRoom7_DiningRoom : Microsoft.Xna.Framework.DrawableGameComponent, MouseInput
    {
        Texture2D windowFrame;
        Texture2D background;
        SpriteBatch spriteBatch;
        Rectangle doorPosition;
        private Texture2D cindy;
        private Rectangle cindyPosition;
        private Texture2D amy;
        private Rectangle amyPosition;
        private LevelHouse house;
        List<String> topText = new List<string>();
        List<Rectangle> topTextPos = new List<Rectangle>();
        List<String> bottomText = new List<string>();
        List<Rectangle> bottomTextPos = new List<Rectangle>();

        private int conversationPtr = -1;
        private Conversation[] conversation = {

            new Conversation(new String[]{}, new String[]{"> Hello Amy. Have you seen my shoe?"}, new int[]{1}), // 0
            new Conversation(new String[]{"Amy: It was a full moon last night."}, new String[]{"> You are not a werewolf!", "> What did you do?"}, new int[]{2, 3}), // 1
            new Conversation(new String[]{"Amy: I was last night!"}, new String[]{"> What did you do?"}, new int[]{3}), // 2
            new Conversation(new String[]{"Amy: Buried it. Sorry."}, new String[]{"> You have to stop doing that!"}, new int[]{-1}), // 3
        };


        public LevelRoom7_DiningRoom(Game game, LevelHouse house)
            : base(game)
        {
            // TODO: Construct any child components here
            this.house = house;
        }

        /// <summary>
        /// Allows the game component to perform any initialization it needs to before starting
        /// to run.  This is where it can query for any required services and load content.
        /// </summary>
        public override void Initialize()
        {
            // TODO: Add your initialization code here

            base.Initialize();

            doorPosition = ((Game1)Game).ScaleToScreen(new Rectangle(890, 84, 223, 299));
        }

        protected override void LoadContent()
        {
            base.LoadContent();
            spriteBatch = new SpriteBatch(Game.GraphicsDevice);
            windowFrame = Game.Content.Load<Texture2D>("window");
            background = Game.Content.Load<Texture2D>("Room7");

            cindy = Game.Content.Load<Texture2D>("cindy");
            cindyPosition = new Rectangle(400, 50, cindy.Bounds.Width, cindy.Bounds.Height);

            amy = Game.Content.Load<Texture2D>("amy");
            amyPosition = new Rectangle(50, 50, amy.Bounds.Width, amy.Bounds.Height);
        }

        Rectangle mousePos;
        bool shouldProcessMousePos = false;
        public void HandleClick(Rectangle pos)
        {
            mousePos = pos;
            shouldProcessMousePos = true;
        }


        /// <summary>
        /// Allows the game component to update itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {
            // TODO: Add your update code here

            base.Update(gameTime);

            if (shouldProcessMousePos)
            {
                topText.Clear();
                bottomText.Clear();

                // User tapped on screen did they hit a window?
                if (bottomTextPos.Count > 0)
                {
                    for (int i = 0; i < bottomTextPos.Count; i++)
                    {
                        if (bottomTextPos[i].Intersects(mousePos))
                        {
                            conversationPtr = conversation[conversationPtr].nextResponse[i];
                        }
                    }
                }
                else if (doorPosition.Intersects(mousePos))
                {
                    ((Game1)Game).ChangeState(house);
                }
                else if (amyPosition.Intersects(mousePos))
                {
                    conversationPtr = 0;
                }
                
                if (conversationPtr > -1)
                {
                    foreach (String s in conversation[conversationPtr].otherPerson)
                    {
                        topText.Add(s);
                    }
                    foreach (String s in conversation[conversationPtr].cindysResponses)
                    {
                        bottomText.Add(s);
                    }
                }

                shouldProcessMousePos = false;
            }
        }

        public override void Draw(GameTime gameTime)
        {
            if (this.Enabled)
            {
                GraphicsDevice.Clear(Color.CornflowerBlue);

                spriteBatch.Begin();
                spriteBatch.Draw(background, GraphicsDevice.Viewport.Bounds, Color.White);
                spriteBatch.Draw(cindy, cindyPosition, Color.White);
                spriteBatch.Draw(amy, amyPosition, Color.White);

                spriteBatch.Draw(windowFrame, GraphicsDevice.Viewport.Bounds, Color.White);
                spriteBatch.End();

                topTextPos = house.DrawTextAtTopOfScreen(topText);
                bottomTextPos = house.DrawTextAtBottomOfScreen(bottomText);

                base.Draw(gameTime);
            }
        }
    }
}
