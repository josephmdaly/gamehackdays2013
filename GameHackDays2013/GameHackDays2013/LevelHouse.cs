using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace GameHackDays2013
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class LevelHouse : Microsoft.Xna.Framework.DrawableGameComponent, MouseInput
    {
        Texture2D house;
        Texture2D white;
        SpriteBatch spriteBatch;
        List<GameComponent> rooms = new List<GameComponent>();
        Rectangle room1Position;
        Rectangle room2Position;
        Rectangle room3Position;
        Rectangle room4Position;
        Rectangle room5Position;
        Rectangle room6Position;
        Rectangle room7Position;
        Rectangle room8Position;
        Rectangle doorPosition;
        SpriteFont arial;

        public LevelHouse(Game game)
            : base(game)
        {
            // TODO: Construct any child components here
        }

        Rectangle mousePos;
        bool shouldProcessMousePos = false;
        public void HandleClick(Rectangle pos)
        {
            mousePos = pos;
            shouldProcessMousePos = true;
        }

        /// <summary>
        /// Allows the game component to perform any initialization it needs to before starting
        /// to run.  This is where it can query for any required services and load content.
        /// </summary>
        public override void Initialize()
        {
            // TODO: Add your initialization code here

            base.Initialize();

            room1Position = ((Game1)Game).ScaleToScreen(new Rectangle(140, 180, 192, 115));
            room2Position = ((Game1)Game).ScaleToScreen(new Rectangle(387, 178, 192, 115));
            room3Position = ((Game1)Game).ScaleToScreen(new Rectangle(680, 178, 192, 115));
            room4Position = ((Game1)Game).ScaleToScreen(new Rectangle(918, 178, 192, 115));
            room5Position = ((Game1)Game).ScaleToScreen(new Rectangle(110, 430, 192, 115));
            room6Position = ((Game1)Game).ScaleToScreen(new Rectangle(336, 434, 192, 115));
            doorPosition = ((Game1)Game).ScaleToScreen(new Rectangle(564, 412, 152, 218));
            room7Position = ((Game1)Game).ScaleToScreen(new Rectangle(735, 435, 192, 115));
            room8Position = ((Game1)Game).ScaleToScreen(new Rectangle(954, 435, 192, 115));
        }

        protected override void LoadContent()
        {
            base.LoadContent();
            spriteBatch = new SpriteBatch(Game.GraphicsDevice);
            white = Game.Content.Load<Texture2D>("White");
            house = Game.Content.Load<Texture2D>("House");
            arial = Game.Content.Load<SpriteFont>("Arial");
        }

        /// <summary>
        /// Allows the game component to update itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {
            // TODO: Add your update code here

            base.Update(gameTime);

            if (shouldProcessMousePos)
            {
                shouldProcessMousePos = false;

                // User tapped on screen did they hit a window?
                if (room1Position.Intersects(mousePos))
                {
                    ((Game1)Game).ChangeState(rooms[0]);
                }
                else if (room2Position.Intersects(mousePos))
                {
                    ((Game1)Game).ChangeState(rooms[1]);
                }
                else if (room3Position.Intersects(mousePos))
                {
                    ((Game1)Game).ChangeState(rooms[2]);
                }
                else if (room4Position.Intersects(mousePos))
                {
                    ((Game1)Game).ChangeState(rooms[3]);
                }
                else if (room5Position.Intersects(mousePos))
                {
                    ((Game1)Game).ChangeState(rooms[4]);
                }
                else if (room6Position.Intersects(mousePos))
                {
                    ((Game1)Game).ChangeState(rooms[5]);
                }
                else if (room7Position.Intersects(mousePos))
                {
                    ((Game1)Game).ChangeState(rooms[6]);
                }
                else if (room8Position.Intersects(mousePos))
                {
                    ((Game1)Game).ChangeState(rooms[7]);
                }
                //else if (doorPosition.Intersects(mousePos))
                //{
                //    ((Game1)Game).ChangeState(rooms[5]);
                //}
            }
        }

        public override void Draw(GameTime gameTime)
        {

            spriteBatch.Begin();
            spriteBatch.Draw(house, GraphicsDevice.Viewport.Bounds, house.Bounds, Color.White);
            spriteBatch.End();

            List<String> textToDraw = new List<string>();
            textToDraw.Add("8 Windows");
            DrawTextAtTopOfScreen(textToDraw);
            //DrawTextAtBottomOfScreen(textToDraw);

            base.Draw(gameTime);
        }

        public List<Rectangle> DrawTextAtTopOfScreen(List<String> textToDraw)
        {
            List<Rectangle> positions = new List<Rectangle>();

            int lastYOffset = 0;
            spriteBatch.Begin();
            foreach (String s in textToDraw)
            {
                if (s == null)
                {
                    continue;
                }

                Vector2 textSize = arial.MeasureString(s);
                Rectangle destRectangle = new Rectangle(0, lastYOffset, (int)textSize.X, (int)textSize.Y);
                lastYOffset += (int)textSize.Y;

                positions.Add(destRectangle);

                spriteBatch.Draw(white, destRectangle, Color.Black);
                spriteBatch.DrawString(arial, s, new Vector2(destRectangle.X, destRectangle.Y),
                    Color.White, 0, Vector2.Zero, 1, SpriteEffects.None, 0);
            }
            spriteBatch.End();

            return positions;
        }


        public List<Rectangle> DrawTextAtBottomOfScreen(List<String> textToDraw)
        {
            List<Rectangle> positions = new List<Rectangle>();

            int lastYOffset = Game.GraphicsDevice.Viewport.Height;
            spriteBatch.Begin();
            for (int i = textToDraw.Count - 1; i >= 0; i--)
            {
                if (textToDraw[i] == null)
                {
                    continue;
                }

                Vector2 textSize = arial.MeasureString(textToDraw[i]);
                lastYOffset -= (int)textSize.Y;
                Rectangle destRectangle = new Rectangle(0, lastYOffset, (int)textSize.X, (int)textSize.Y);

                positions.Add(destRectangle);

                spriteBatch.Draw(white, destRectangle, Color.Black);
                spriteBatch.DrawString(arial, textToDraw[i], new Vector2(destRectangle.X, destRectangle.Y),
                    Color.White, 0, Vector2.Zero, 1, SpriteEffects.None, 0);
            }
            spriteBatch.End();

            positions.Reverse();

            return positions;
        }

        internal void setRooms(DrawableGameComponent room1,
            DrawableGameComponent room2,
            DrawableGameComponent room3,
            DrawableGameComponent room4,
            DrawableGameComponent room5,
            DrawableGameComponent room6,
            DrawableGameComponent room7,
            DrawableGameComponent room8)
        {
            rooms.Add(room1);
            rooms.Add(room2);
            rooms.Add(room3);
            rooms.Add(room4);
            rooms.Add(room5);
            rooms.Add(room6);
            rooms.Add(room7);
            rooms.Add(room8);
        }
    }
}
