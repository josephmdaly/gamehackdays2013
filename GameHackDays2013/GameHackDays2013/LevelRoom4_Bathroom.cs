using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace GameHackDays2013
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class LevelRoom4_Bathroom : Microsoft.Xna.Framework.DrawableGameComponent, MouseInput
    {
        Texture2D windowFrame;
        Texture2D background;
        Texture2D cindy;
        SpriteBatch spriteBatch;
        Rectangle doorPosition;
        Rectangle bathPosition;
        Rectangle sinkPosition;
        Rectangle cindyPosition;
        private LevelHouse house;
        List<String> topText = new List<string>();
        List<Rectangle> topTextPos = new List<Rectangle>();
        List<String> bottomText = new List<string>();
        List<Rectangle> bottomTextPos = new List<Rectangle>();

        public LevelRoom4_Bathroom(Game game, LevelHouse house)
            : base(game)
        {
            // TODO: Construct any child components here
            this.house = house;
        }

        /// <summary>
        /// Allows the game component to perform any initialization it needs to before starting
        /// to run.  This is where it can query for any required services and load content.
        /// </summary>
        public override void Initialize()
        {
            // TODO: Add your initialization code here

            base.Initialize();

            doorPosition = ((Game1)Game).ScaleToScreen(new Rectangle(890, 84, 223, 299));
            bathPosition = ((Game1)Game).ScaleToScreen(new Rectangle(16, 288, 582, 151));
            sinkPosition = ((Game1)Game).ScaleToScreen(new Rectangle(675, 225, 137, 201));
        }

        protected override void LoadContent()
        {
            base.LoadContent();
            spriteBatch = new SpriteBatch(Game.GraphicsDevice);
            windowFrame = Game.Content.Load<Texture2D>("window");
            background = Game.Content.Load<Texture2D>("Room4");

            cindy = Game.Content.Load<Texture2D>("cindy");
            cindyPosition = new Rectangle(50, 50, cindy.Bounds.Width, cindy.Bounds.Height);
        }

        Rectangle mousePos;
        bool shouldProcessMousePos = false;
        public void HandleClick(Rectangle pos)
        {
            mousePos = pos;
            shouldProcessMousePos = true;
        }


        /// <summary>
        /// Allows the game component to update itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {
            // TODO: Add your update code here

            base.Update(gameTime);

            if (shouldProcessMousePos)
            {
                topText.Clear();
                bottomText.Clear();

                // User tapped on screen did they hit a window?
                if (doorPosition.Intersects(mousePos))
                {
                    ((Game1)Game).ChangeState(house);
                }
                else if (bathPosition.Intersects(mousePos))
                {
                    topText.Add("Cindy: I'm already clean");
                }
                else if (sinkPosition.Intersects(mousePos))
                {
                    topText.Add("Cindy: I always wash my hands");
                }
                shouldProcessMousePos = false;
            }
        }

        public override void Draw(GameTime gameTime)
        {
            if (this.Enabled)
            {
                GraphicsDevice.Clear(Color.CornflowerBlue);

                spriteBatch.Begin();
                spriteBatch.Draw(background, GraphicsDevice.Viewport.Bounds, Color.White);
                spriteBatch.Draw(cindy, cindyPosition, Color.White);
                spriteBatch.Draw(windowFrame, GraphicsDevice.Viewport.Bounds, Color.White);
                spriteBatch.End();

                topTextPos = house.DrawTextAtTopOfScreen(topText);
                bottomTextPos = house.DrawTextAtBottomOfScreen(bottomText);

                base.Draw(gameTime);
            }
        }
    }
}
