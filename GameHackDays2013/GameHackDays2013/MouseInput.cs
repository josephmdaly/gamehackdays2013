using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameHackDays2013
{
    interface MouseInput
    {
        void HandleClick(Rectangle pos);
    }
}
