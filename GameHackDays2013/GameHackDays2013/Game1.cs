using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Input.Touch;
using Microsoft.Xna.Framework.Media;

namespace GameHackDays2013
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        GameComponent currentState;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            // Frame rate is 30 fps by default for Windows Phone.
            TargetElapsedTime = TimeSpan.FromTicks(333333);

            // Extend battery life under lock.
            InactiveSleepTime = TimeSpan.FromSeconds(1);

            var houseLevel = new LevelHouse(this);
            var room1 = new LevelRoom1_Bedroom(this, houseLevel);
            var room2 = new LevelRoom2_FriendsRoom1(this, houseLevel);
            var room3 = new LevelRoom3_FriendsRoom2(this, houseLevel);
            var room4 = new LevelRoom4_Bathroom(this, houseLevel);
            var room5 = new LevelRoom5_LivingRoom(this, houseLevel);
            var room6 = new LevelRoom6_Hall(this, houseLevel);
            var room7 = new LevelRoom7_DiningRoom(this, houseLevel);
            var room8 = new LevelRoom8_Kitchen(this, houseLevel);

            ChangeState(houseLevel);            

            houseLevel.setRooms(room1, room2, room3, room4, room5, room6, room7, room8);
        }

        public void ChangeState(GameComponent newState)
        {
            this.Components.Remove(currentState);
            currentState = newState;
            this.Components.Add(currentState);
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }


        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        bool processedMouseDown = false;
        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();

            // TODO: Add your update logic here
            var mState = Mouse.GetState();

            if (mState.LeftButton == ButtonState.Pressed && processedMouseDown == false)
            {
                processedMouseDown = true;

                // User tapped on screen did they hit a window?
                var mousePos = new Rectangle(mState.X, mState.Y, 1, 1);
                ((MouseInput)currentState).HandleClick(mousePos);
            }
            else if (mState.LeftButton == ButtonState.Released)
            {
                processedMouseDown = false;
            }

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here

            base.Draw(gameTime);
        }

        /// <summary>
        /// Convert screen coordinates to match the current screen size
        /// </summary>
        /// <param name="fullSizeVal"></param>
        /// <returns></returns>
        public Rectangle ScaleToScreen(Rectangle fullSizeVal)
        {
            float screenWidth = this.GraphicsDevice.Viewport.Width;
            float screenHeight = this.GraphicsDevice.Viewport.Height;

            float assetWidth = 1280;
            float assetHeight = 768;

            return new Rectangle(
                (int)Math.Ceiling((fullSizeVal.X / assetWidth) * screenWidth),
                (int)Math.Ceiling((fullSizeVal.Y / assetHeight) * screenHeight),
                (int)Math.Ceiling((fullSizeVal.Width / assetWidth) * screenWidth),
                (int)Math.Ceiling((fullSizeVal.Height / assetHeight) * screenHeight));
        }
    }
}
