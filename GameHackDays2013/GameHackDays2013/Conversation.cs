using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameHackDays2013
{
    class Conversation
    {
        public String[] otherPerson = {};
        public String[] cindysResponses = { };
        public int[] nextResponse = { };

        public Conversation(String[] otherPerson, String[] cindysResponses, int[] nextResponse)
        {
            this.otherPerson = otherPerson;
            this.cindysResponses = cindysResponses;
            this.nextResponse = nextResponse;
        }
    }
}
