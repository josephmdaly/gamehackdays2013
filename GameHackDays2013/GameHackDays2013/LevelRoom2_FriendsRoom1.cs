using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace GameHackDays2013
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class LevelRoom2_FriendsRoom1 : Microsoft.Xna.Framework.DrawableGameComponent, MouseInput
    {
        private Texture2D windowFrame;
        private Texture2D background;
        private Texture2D cindy;
        private Texture2D jill;
        private SpriteBatch spriteBatch;
        private Rectangle doorPosition;
        private Rectangle bedPosition;
        private Rectangle cindyPosition;
        private Rectangle jillPosition;
        private LevelHouse house;
        private List<String> topText = new List<string>();
        private List<Rectangle> topTextPos = new List<Rectangle>();
        private List<String> bottomText = new List<string>();
        private List<Rectangle> bottomTextPos = new List<Rectangle>();

        private int conversationPtr = -1;
        private Conversation[] conversation = {

            new Conversation(new String[]{}, new String[]{"> Hello Jill. Have you seen my shoe?"}, new int[]{1}), // 0
            new Conversation(new String[]{"Jill: The one you threw over the house?"}, new String[]{"> What? When did that happen?", "> I was always good at sport.", "> Which house?"}, new int[]{ 2, 3, 4}), // 1
            new Conversation(new String[]{"Jill: Last night. Drunken rampage.", "Jill: Ask Amy."}, new String[]{"> OK. I'll ask Amy. Thanks."}, new int[]{-1}), // 2
            new Conversation(new String[]{"Jill: You missed.", "Jill: Talk to Amy. She fetched it."}, new String[]{"> OK. I'll ask Amy. Thanks."}, new int[]{-1}), // 3
            new Conversation(new String[]{"Jill: This one.", "Jill: Amy went to fetch it."}, new String[]{"> OK. I'll ask Amy. Thanks."}, new int[]{-1}), // 4
        };

        public LevelRoom2_FriendsRoom1(Game game, LevelHouse house)
            : base(game)
        {
            // TODO: Construct any child components here
            this.house = house;
        }

        /// <summary>
        /// Allows the game component to perform any initialization it needs to before starting
        /// to run.  This is where it can query for any required services and load content.
        /// </summary>
        public override void Initialize()
        {
            // TODO: Add your initialization code here

            base.Initialize();

            doorPosition = ((Game1)Game).ScaleToScreen(new Rectangle(890, 84, 223, 299));
            bedPosition = ((Game1)Game).ScaleToScreen(new Rectangle(0, 294, 851, 322));
        }

        protected override void LoadContent()
        {
            base.LoadContent();
            spriteBatch = new SpriteBatch(Game.GraphicsDevice);
            windowFrame = Game.Content.Load<Texture2D>("window");
            background = Game.Content.Load<Texture2D>("Room2");
            cindy = Game.Content.Load<Texture2D>("cindy");
            jill = Game.Content.Load<Texture2D>("jill");

            cindyPosition = new Rectangle(400, 50, cindy.Bounds.Width, cindy.Bounds.Height);
            jillPosition = new Rectangle(50, 50, jill.Bounds.Width, jill.Bounds.Height);
        }

        Rectangle mousePos;
        bool shouldProcessMousePos = false;
        public void HandleClick(Rectangle pos)
        {
            mousePos = pos;
            shouldProcessMousePos = true;
        }


        /// <summary>
        /// Allows the game component to update itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {
            // TODO: Add your update code here

            base.Update(gameTime);

            if (shouldProcessMousePos)
            {
                topText.Clear();
                bottomText.Clear();

                if (bottomTextPos.Count > 0)
                {
                    for (int i = 0; i < bottomTextPos.Count; i++)
                    {
                        if (bottomTextPos[i].Intersects(mousePos))
                        {
                            conversationPtr = conversation[conversationPtr].nextResponse[i];
                        }
                    }
                } else if (doorPosition.Intersects(mousePos))
                {
                    ((Game1)Game).ChangeState(house);
                }
                else if (jillPosition.Intersects(mousePos))
                {
                    conversationPtr = 0;

                }
                //else if (cindyPosition.Intersects(mousePos))
                //{
                //    topText.Add("Cindy: Me again! Hi!");
                //}
                else if (bedPosition.Intersects(mousePos))
                {
                    topText.Add("Cindy: Jills bed.");
                }

                if (conversationPtr > -1)
                {
                    foreach (String s in conversation[conversationPtr].otherPerson)
                    {
                        topText.Add(s);
                    }
                    foreach (String s in conversation[conversationPtr].cindysResponses)
                    {
                        bottomText.Add(s);
                    }
                }
                
                shouldProcessMousePos = false;
            }
        }

        public override void Draw(GameTime gameTime)
        {
            if (this.Enabled)
            {
                GraphicsDevice.Clear(Color.CornflowerBlue);

                spriteBatch.Begin();
                spriteBatch.Draw(background, GraphicsDevice.Viewport.Bounds, Color.White);
                spriteBatch.Draw(jill, jillPosition, Color.White);
                spriteBatch.Draw(cindy, cindyPosition, Color.White);
                spriteBatch.Draw(windowFrame, GraphicsDevice.Viewport.Bounds, Color.White);
                spriteBatch.End();

                topTextPos = house.DrawTextAtTopOfScreen(topText);
                bottomTextPos = house.DrawTextAtBottomOfScreen(bottomText);

                base.Draw(gameTime);
            }
        }
    }
}
