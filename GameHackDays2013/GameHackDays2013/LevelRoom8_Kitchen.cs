using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace GameHackDays2013
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class LevelRoom8_Kitchen : Microsoft.Xna.Framework.DrawableGameComponent, MouseInput
    {
        Texture2D windowFrame;
        Texture2D background;
        SpriteBatch spriteBatch;
        Rectangle doorPosition;
        private Texture2D cindy;
        private Rectangle cindyPosition;
        private Rectangle tapPosition;
        private Rectangle ovenPosition;
        private LevelHouse house;
        List<String> topText = new List<string>();
        List<Rectangle> topTextPos = new List<Rectangle>();
        List<String> bottomText = new List<string>();
        List<Rectangle> bottomTextPos = new List<Rectangle>();

        public LevelRoom8_Kitchen(Game game, LevelHouse house)
            : base(game)
        {
            // TODO: Construct any child components here
            this.house = house;
        }

        /// <summary>
        /// Allows the game component to perform any initialization it needs to before starting
        /// to run.  This is where it can query for any required services and load content.
        /// </summary>
        public override void Initialize()
        {
            // TODO: Add your initialization code here

            base.Initialize();

            doorPosition = ((Game1)Game).ScaleToScreen(new Rectangle(890, 84, 223, 299));
            tapPosition = ((Game1)Game).ScaleToScreen(new Rectangle(351, 148, 248, 268));
            ovenPosition = ((Game1)Game).ScaleToScreen(new Rectangle(612, 218, 235, 192));
        }

        protected override void LoadContent()
        {
            base.LoadContent();
            spriteBatch = new SpriteBatch(Game.GraphicsDevice);
            windowFrame = Game.Content.Load<Texture2D>("window");
            background = Game.Content.Load<Texture2D>("Room8");

            cindy = Game.Content.Load<Texture2D>("cindy");
            cindyPosition = new Rectangle(50, 50, cindy.Bounds.Width, cindy.Bounds.Height);

        }

        Rectangle mousePos;
        bool shouldProcessMousePos = false;
        public void HandleClick(Rectangle pos)
        {
            mousePos = pos;
            shouldProcessMousePos = true;
        }


        /// <summary>
        /// Allows the game component to update itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {
            // TODO: Add your update code here

            base.Update(gameTime);

            if (shouldProcessMousePos)
            {
                topText.Clear();
                bottomText.Clear();

                // User tapped on screen did they hit a window?
                if (doorPosition.Intersects(mousePos))
                {
                    ((Game1)Game).ChangeState(house);
                }
                else if (tapPosition.Intersects(mousePos))
                {
                    topText.Add("Cindy: Thanks! I was thirsty.");
                }
                else if (ovenPosition.Intersects(mousePos))
                {
                    topText.Add("Cindy: My shoe is not in the oven.");
                    topText.Add("Cindy: That was my best hope :(");
                }
                shouldProcessMousePos = false;
            }
        }

        public override void Draw(GameTime gameTime)
        {
            if (this.Enabled)
            {
                GraphicsDevice.Clear(Color.CornflowerBlue);

                spriteBatch.Begin();
                spriteBatch.Draw(background, GraphicsDevice.Viewport.Bounds, Color.White);
                spriteBatch.Draw(cindy, cindyPosition, Color.White);

                spriteBatch.Draw(windowFrame, GraphicsDevice.Viewport.Bounds, Color.White);
                spriteBatch.End();

                topTextPos = house.DrawTextAtTopOfScreen(topText);
                bottomTextPos = house.DrawTextAtBottomOfScreen(bottomText);

                base.Draw(gameTime);
            }
        }
    }
}
